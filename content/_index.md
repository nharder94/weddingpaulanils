---
# Striking header background image, Ideal images are homogenous around the centre and contrasting to the text. Non-ideal images can use `title_guard`
header_image: "images/sundown.jpg"
#title_guard: "images/sundown.jpg"
#
# When set true, uses video from custom_header_video.html partial, instead of header_image
header_use_video: false
#
# Optional header logo. CSS: `#blog-logo`, with max-height defined, optimize to prevent scaling
header_logo: "images/flowers.png"
#
# Headers are safeHTML, you can use HTML tags such as b,i,u,br
header_headline: "Es ist soweit"
header_subheadline: "Paula und Nils in trauter Zweisamkeit vereint!"

# Add a 'Go back to top' item to the navigation menu
# Title: name of navigation menu entry
# Weight (i. e. position in menu): none = no menu entry, first = add as first entry, last = ad as last entry
nav_to_top_title: "Nochmal von vorn?"
nav_to_top_weight: none
---
