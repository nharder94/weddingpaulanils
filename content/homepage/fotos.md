---
title: "Fotos!"
#header_menu_title: "Services"
#navigation_menu_title: "My Services"
weight: 3.5
header_menu: true
---

Nachdem wir unsere Hochzeit gebührend gefeiert und die Flitterwoche heile überstanden haben, würden wir gerne 
mit euch allen in Erinnerungen an den schönen Tag schwelgen. 

Dafür haben wir euch auf anderen Wegen einen Link zukommen lassen, mit dem ihr Bilder hochladen könnt, die dann
auf magische Art und Weise hier erscheinen werden - natürlich nur wenn man das geheime Passwort kennt. 

Eure peinlichen Momente bleiben also dem Rest des Internets verborgen ;)


---

<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="faxziznjeqlmi8g"></script>

<div style="width:100%;height:640px" id="folder_container"></div>

<!--a href="https://www.dropbox.com/scl/fo/btypsm7a2du5w6b8url8p/ADfT9eseaIp4nwXzVA6Kb_A?rlkey=66ulu6hzrx89hiptqsogpuwmx&st=8ezvlhhf&dl=0"
  class="dropbox-embed"
></a-->

<script type="text/javascript">
var options = {
	// Shared link to Dropbox file
	link: "https://www.dropbox.com/scl/fo/btypsm7a2du5w6b8url8p/ADfT9eseaIp4nwXzVA6Kb_A?rlkey=66ulu6hzrx89hiptqsogpuwmx&st=tv31obz1&dl=0",
	file: {
	// Sets the zoom mode for embedded files. Defaults to 'best'.
	zoom: "best" // or "fit"
	},
	folder: {
	// Sets the view mode for embedded folders. Defaults to 'list'.
	view: "grid", // or "list"
	headerSize: "normal" // or "small"
	}
}
var element = document.getElementById("folder_container");
Dropbox.embed(options, element);
</script>
