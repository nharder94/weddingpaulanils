---
title: "Hochzeit!"
#header_menu_title: "Services"
#navigation_menu_title: "My Services"
weight: 2
header_menu: true
---

Ja, eine richtige Hochzeit, mit Standesamt und Fotos und Party bis spät in die Nacht soll es werden.

Und wenn ihr diese Website gefunden habt, seid ihr bestimmt auch eingeladen worden :)

Für eventuelle Party-aktivitäten meldet euch bitte, wie in der Einladung schon geschrieben, bei Hauke, Leonie oder Katharina.


