---
title: "Meldestelle"
weight: 7
header_menu: true
---
Meldet euch bitte zurück damit wir die Gästeanzahl für den Veranstalter und das Catering festlegen können - nicht das am Ende jemand verhungern muss!

Ihr habt bestimmt irgendeine gültige Telefonnummer von einem von uns, dort sind wir beide telefonisch, auf Whatsapp und Signal erreichbar.

Wer es lieber förmlich mag kann sich auch gerne an diese frisch geschmiedete Email-Adresse wenden, da werden wir mindestens einmal am Tag reingucken.

{{<icon class="fa fa-envelope">}}&nbsp;[{{<email>}}](mailto:{{<email>}})

Natürlich könnt ihr auch gern einen Brief zurückschreiben, auf allen Einladungsumschlägen steht die Sende-Adresse und Lotte liebt es Briefe aufzumachen :D




