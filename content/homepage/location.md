---
title: "Wo?"
#header_menu_title: "Services"
#navigation_menu_title: "My Services"
weight: 3
header_menu: true
---

Eine besondere Feier benötigt einen besonderen Ort, und wir haben uns für das Gut Zichtau entschieden.

Etwas ab vom Schuss, aber dafür sehr schön und ruhig sollte es sowohl drinnen als auch draußen einen passenden Rahmen  für jegliche Hochzeiterei bieten.

Um 15:30 Uhr wird der Kuchen aufgetischt - seid also nicht zu spät, sonst besteht die Gefahr, dass ich genau euren Lieblingskuchen schon aufgegessen habe!

Für alle die ohne eigenes Auto anreisen wollen bekommen wir bestimmt Fahrservice von/nach Magdeburg Hbf Organisiert, meldet euch einfach.

---

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2422.9594963289082!2d11.29480037743295!3d52.60650297208399!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47af0ce5752fc717%3A0xd8a9dd33b0b93697!2sGut%20Zichtau%20-%20Ihre%20Eventlocation%20f%C3%BCr%20Hochzeiten%2C%20Tagungen%20und%20Feiern%20in%20der%20Altmark!5e0!3m2!1sde!2sde!4v1709765763698!5m2!1sde!2sde" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
