---
title: "Kuchen?!"
weight: 5
header_menu: true
---

Wie schon in der Einladung geschrieben, könnt ihr gerne Kuchen mitbringen, sagt uns aber am besten vorher Bescheid.

Damit am Ende nicht 10 russische Zupfkuchen auf der Kuchentafel stehen steht hier eine vorläufige Liste aller angekündigten Kuchen:

- Ein Blech Schneewittchenkuchen
- Ein geheimnisvoller Kuchen mit Frischkäse (schon getestet, sehr lecker!)
- Brownies
- Biskuitrolle
- Himbeer-Schmand Torte
- Macarons
- Eierlikör-Muffins
- Schokokuchen
- Cheesecake
- Russischer Zupfkuchen (♥️)
- Apfelkuchen
- Zitronenkuchen
- Veganer Schmandkuchen
- Käsekuchen

