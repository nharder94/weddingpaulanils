---
title: "Und danach?"
#header_menu_title: "Services"
#navigation_menu_title: "My Services"
weight: 4
header_menu: true
---

Übernachtungsplätze finden sich am besten im Nachbarort Klötze, bei zwei Hotels haben wir schon bescheid gesagt unter dem Namen
> Hochzeit Harder

wir würden uns drum kümmern, dass Nachts alle heile in ihr Hotel kommen.

In Zichtau direkt gibt es noch direkt neben dem Festsaal ein paar wenige Betten die wir gerne an die Gäste verteilen würden die mit Kind und/oder Kegel anreisen wollen,
die Zimmer sollten theoretisch in Babyfon-Reichweite zur Party sein. 

In Zichtau direkt gibt es auch noch ein Feriendorf in Laufreichweite, leider hat dort eine andere Feier erstmal alle Zimmer blockiert und gibt diese erst Anfang/Mitte April wieder frei - 
sobald ich da näheres weiß aktualisiere ich hier nochmal die Adressen und melde mich.

---
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.4300665929827!2d11.176128710797947!3d52.627204092545334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47afa6e9503e6cfb%3A0xc3bed9af0dc1ac56!2sG%C3%A4stehaus%20Karin%2FPension%2FApartments%2FHotel!5e0!3m2!1sde!2sde!4v1709765873136!5m2!1sde!2sde" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5436.875728004628!2d11.168142660545971!3d52.62650277323945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47afa6bd0e5dda33%3A0xe70091a61c4d9472!2sBraunschweiger%20Hof!5e0!3m2!1sde!2sde!4v1709765903848!5m2!1sde!2sde" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
