---
title: "Katerfrühstück"
#header_menu_title: "Services"
#navigation_menu_title: "My Services"
weight: 6
header_menu: true
---

Auf dem Gut Zichtau gibt es auch einen kleinen Frühstückssaal und die Vermieter haben uns angeboten am morgen nach der Feier noch ein Frühstück mit Restebuffet und Filterkaffee zu veranstalten.

Falls ihr gerne mit auskatern wollt meldet euch bitte, damit wir dem Vermieter Bescheid geben können wie viele Tische vorbereitet werden sollen und wie viele Dutzend Brötchen wir organiseren müssen.
